# acs-mercado-copy 🛒
![Jest unit tests](https://github.com/Lucheti/acs-mercado-copy/workflows/Jest%20unit%20tests/badge.svg?branch=master)
![Cypress end-to-end tests](https://github.com/Lucheti/acs-mercado-copy/workflows/Cypress%20end-to-end%20tests/badge.svg?branch=master)

To run with docker :whale:
- sudo docker-compose build
- sudo docker-compose up
